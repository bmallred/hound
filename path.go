package hound

import (
	"image"
)

// Path is an array of points.
type Path []image.Point

// CostFunc returns the cost of going from point A to point B.
type CostFunc func(a, b image.Point) float64

// NewPath creates a new path with a starting point.
func NewPath(point image.Point) Path {
	return []image.Point{point}
}

// Last returns the last point in the path.
func (p Path) Last() image.Point {
	return p[len(p)-1]
}

// Continue adds a point to the path and returns the path.
func (p Path) Continue(point image.Point) Path {
	next := make([]image.Point, len(p), len(p)+1)
	copy(next, p)
	next = append(next, point)
	return next
}

// Cost applies the cost function to each point on the path.
func (p Path) Cost(f CostFunc) float64 {
	cost := float64(0)
	for i := 1; i < len(p); i++ {
		cost += f(p[i-1], p[i])
	}
	return cost
}
