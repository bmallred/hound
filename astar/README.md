# A* (or A-star) search algorithm

> A* (pronounced "A-star") is a graph traversal and path search algorithm, which is often used in many fields of computer science due to its completeness, optimality, and optimal efficiency.[1] One major practical drawback is its O ( b d ) {\displaystyle O(b^{d})} O(b^d) space complexity, as it stores all generated nodes in memory. Thus, in practical travel-routing systems, it is generally outperformed by algorithms which can pre-process the graph to attain better performance,[2] as well as memory-bounded approaches; however, A* is still the best solution in many cases.[3]
>
> Peter Hart, Nils Nilsson and Bertram Raphael of Stanford Research Institute (now SRI International) first published the algorithm in 1968.[4] It can be seen as an extension of Edsger Dijkstra's 1959 algorithm. A* achieves better performance by using heuristics to guide its search. 
[Wikipedia](https://en.wikipedia.org/wiki/A*_search_algorithm)

## Neighbors

In order to build the path we need to provide a way to present options to our algorithm so it may then do an analysis based on the deterministic and heuristic cost values. A generic function type can be defined as such

``` go "astar neighbors"
// NeighborFunc returns neighbors from a point on a graph.
type NeighborsFunc func(graph [][]int, point image.Point) []image.Point
```

This will depend on the `image` package

``` go "astar imports"
"image"
```

### The four cardinal directions

To begin we will only declare the standard four cardinal directions of North, East, South, and West:

``` go "astar neighbors" +=
// NeighborsCardinalFour returns neighbors on a graph only in the four cardinal directions.
func NeighborsCardinalFour(graph [][]int, point image.Point) []image.Point {
	n := []image.Point{}
	maxX := len(graph[0]) - 1
	maxY := len(graph) - 1

	north := image.Pt(point.X, point.Y-1)
	if north.Y >= 0 {
		n = append(n, north)
	}

	east := image.Pt(point.X+1, point.Y)
	if east.X <= maxX {
		n = append(n, east)
	}

	south := image.Pt(point.X, point.Y+1)
	if south.Y <= maxY {
		n = append(n, south)
	}

	west := image.Pt(point.X-1, point.Y)
	if west.X >= 0 {
		n = append(n, west)
	}

	return n
}
```

### The nine cardinal directions

If diagonal movement is allowed then it may be beneficial to accomodate for this by extending the standard four cardinal directions.

``` go "astar neighbors" +=
// NeighborsCardinalNine returns neighbors on a graph only in the nine cardinal directions.
func NeighborsCardinalNine(graph [][]int, point image.Point) []image.Point {
	n := NeighborsCardinalFour(graph, point)
	maxX := len(graph[0]) - 1
	maxY := len(graph) - 1

	northeast := image.Pt(point.X+1, point.Y-1)
	if northeast.Y >= 0 && northeast.X <= maxX {
		n = append(n, northeast)
	}

	northwest := image.Pt(point.X-1, point.Y-1)
	if northwest.Y >= 0 && northwest.X >= 0 {
		n = append(n, northwest)
	}

	southeast := image.Pt(point.X+1, point.Y+1)
	if southeast.Y <= maxY && southeast.X <= maxX {
		n = append(n, southeast)
	}

	southwest := image.Pt(point.X-1, point.Y+1)
	if southwest.Y <= maxY && southwest.X >= 0 {
		n = append(n, southwest)
	}

	return n
}
```

## Finding the path

Since we will be using code from the main `hound` library go ahead and declare it is an import.

``` go "astar imports" +=
"gitlab.com/bmallred/hound"
```

The signature of our method will have the following:

 - `graph`
 - `source` and `target`
 - `neighborsFunc`
 - `deterministic` and `heuristic`

```go "astar path"
// FindPath returns the optimal path from the source to the target points of a graph according to the cost functions.
func FindPath(graph [][]int, source, target image.Point, neighborsFunc NeighborsFunc, deterministic, heuristic hound.CostFunc) hound.Path {
<<<astar algorithm>>>
}
```

First, we will need to track any closed paths so we do not backtrack.

``` go "astar algorithm"
closed := make(map[image.Point]bool)
```

This algorithm also makes use of a heap stack so we will import the following package

``` go "astar imports" +=
"container/heap"
```

and declare our heap while pushing the initial path starting at the source coordinates.

``` go "astar algorithm" +=
pq := &hound.PriorityQueue{}
heap.Init(pq)
heap.Push(pq, &hound.Item{Value: hound.NewPath(source)})
```

Then we will continue to loop through the queue while there are still items to be processed. If it exits then return a `nil` value indicating there was nothing done.

``` go "astar algorithm" +=
for pq.Len() > 0 {
	<<<astar process queued item>>>
}

return nil
```

For each item from the queue we will first `Pop` the item from the queue which is declared as a value type of `hound.Path`.

``` go "astar process queued item"
p := heap.Pop(pq).(*hound.Item).Value.(hound.Path)
```

In addition we want to get the last point from the path

``` go "astar process queued item" +=
n := p.Last()
```

and see if it has alread been closed. If it has then continue to the next queued item

``` go "astar process queued item" +=
// Skip if already closed
if closed[n] {
	continue
}
```

If it has not been closed, but it is equal to the target destination then we have found our path and will return the result.

``` go "astar process queued item" +=
// Return if the path was found
if n == target {
	return p
}
```

Otherwise mark it as closed

``` go "astar process queued item" +=
// Mark this point as closed
closed[n] = true
```

Now the algorithm must make a decision as to which path to take next. To do this it calls the function provided to return back all valid neighboring coordinates. From these results it will add them to the path. Then we will add a new queued item to the heap with the next path and its priority being the overall deterministic cost of the path in addition to the heuristic cost of the current coordinate to the target destination.

``` go "astar process queued item" +=
for _, nb := range neighborsFunc(graph, n) {
	next := p.Continue(nb)
	heap.Push(pq, &hound.Item{
		Value:    next,
		Priority: -(next.Cost(deterministic) + heuristic(nb, target)),
	})
}
```

## File structure

In this section we define the basic file structure used.

The package is called `astar` so it will need to go in to each file.

``` go "package name"
package astar
```

### astar.go

``` go astar.go
<<<package name>>>
import (
	<<<astar imports>>>
)
<<<astar neighbors>>>
<<<astar path>>>
```
