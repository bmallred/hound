package astar

import (
	"container/heap"
	"gitlab.com/bmallred/hound"
	"image"
)

// NeighborFunc returns neighbors from a point on a graph.
type NeighborsFunc func(graph [][]int, point image.Point) []image.Point

// NeighborsCardinalFour returns neighbors on a graph only in the four cardinal directions.
func NeighborsCardinalFour(graph [][]int, point image.Point) []image.Point {
	n := []image.Point{}
	maxX := len(graph[0]) - 1
	maxY := len(graph) - 1

	north := image.Pt(point.X, point.Y-1)
	if north.Y >= 0 {
		n = append(n, north)
	}

	east := image.Pt(point.X+1, point.Y)
	if east.X <= maxX {
		n = append(n, east)
	}

	south := image.Pt(point.X, point.Y+1)
	if south.Y <= maxY {
		n = append(n, south)
	}

	west := image.Pt(point.X-1, point.Y)
	if west.X >= 0 {
		n = append(n, west)
	}

	return n
}

// NeighborsCardinalNine returns neighbors on a graph only in the nine cardinal directions.
func NeighborsCardinalNine(graph [][]int, point image.Point) []image.Point {
	n := NeighborsCardinalFour(graph, point)
	maxX := len(graph[0]) - 1
	maxY := len(graph) - 1

	northeast := image.Pt(point.X+1, point.Y-1)
	if northeast.Y >= 0 && northeast.X <= maxX {
		n = append(n, northeast)
	}

	northwest := image.Pt(point.X-1, point.Y-1)
	if northwest.Y >= 0 && northwest.X >= 0 {
		n = append(n, northwest)
	}

	southeast := image.Pt(point.X+1, point.Y+1)
	if southeast.Y <= maxY && southeast.X <= maxX {
		n = append(n, southeast)
	}

	southwest := image.Pt(point.X-1, point.Y+1)
	if southwest.Y <= maxY && southwest.X >= 0 {
		n = append(n, southwest)
	}

	return n
}

// FindPath returns the optimal path from the source to the target points of a graph according to the cost functions.
func FindPath(graph [][]int, source, target image.Point, neighborsFunc NeighborsFunc, deterministic, heuristic hound.CostFunc) hound.Path {
	closed := make(map[image.Point]bool)
	pq := &hound.PriorityQueue{}
	heap.Init(pq)
	heap.Push(pq, &hound.Item{Value: hound.NewPath(source)})
	for pq.Len() > 0 {
		p := heap.Pop(pq).(*hound.Item).Value.(hound.Path)
		n := p.Last()
		// Skip if already closed
		if closed[n] {
			continue
		}
		// Return if the path was found
		if n == target {
			return p
		}
		// Mark this point as closed
		closed[n] = true
		for _, nb := range neighborsFunc(graph, n) {
			next := p.Continue(nb)
			heap.Push(pq, &hound.Item{
				Value:    next,
				Priority: -(next.Cost(deterministic) + heuristic(nb, target)),
			})
		}
	}

	return nil
}
