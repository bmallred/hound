# Hound

## Description

A library used in 2D Roguelike games for pathfinding purposes. There are no dependencies outside of the standard library in this pure Go implementation.

## Algorithms found in this library

 - [A*](astar/)
 
## Mission statement

Then end goal is to reduce the influence on codebases implementing this library by leveraging the Go standard library and primitives. If another project needs to undergo a sizeable refactoring process then the project has failed.

## The nature of this document

This program was converted to **literate programming** with the help of [lmt](https://github.com/driusan/lmt) by **Dave MacFarlane**.

To build from this document you may do the following:

``` shell
lmt README.md;
gofmt -w *.go;
```

## What is a path?

A path is an array of coordinates, in our use case we leverage `image.Point`, which may be taken to arrive at a destination from the provided position.

``` go "path types"
// Path is an array of points.
type Path []image.Point
```

What is nice about this is we will only need to leverage the `image` package from the standard libary

``` go "path import"
import (
	"image"
)
```

A `Path` has a few helper functions as well. For instance,`NewPath` creates a new `Path` starting from a provided `point`

``` go "path methods"
// NewPath creates a new path with a starting point.
func NewPath(point image.Point) Path {
	return []image.Point{point}
}
```

It is also possible to return the last point for a path with the following:

``` go "path methods" +=
// Last returns the last point in the path.
func (p Path) Last() image.Point {
	return p[len(p)-1]
}
```

Or, to add another point to the path and continue

``` go "path methods" +=
// Continue adds a point to the path and returns the path.
func (p Path) Continue(point image.Point) Path {
	next := make([]image.Point, len(p), len(p)+1)
	copy(next, p)
	next = append(next, point)
	return next
}
```

## What is a cost?

``` go "path types" +=
// CostFunc returns the cost of going from point A to point B.
type CostFunc func(a, b image.Point) float64
```

``` go "path methods" +=
// Cost applies the cost function to each point on the path.
func (p Path) Cost(f CostFunc) float64 {
	cost := float64(0)
	for i := 1; i < len(p); i++ {
		cost += f(p[i-1], p[i])
	}
	return cost
}
```

## What is a graph?

In 2D games a graph can be a two-dimensional array of integers representing the level.

## File structure

In this section we define the basic file structure used.

The base package is called `hound` so it will need to go in to each file.

``` go "package name"
package hound
```

### Path

``` go path.go
<<<package name>>>
<<<path import>>>
<<<path types>>>
<<<path methods>>>
```
